FROM golang:1.8-alpine

# build args and environment
ARG SRC=$GOPATH/src
ENV NAME=""
ENV PROTODIR=""
ENV CODE=$SRC/code

# install requrements
RUN apk add --update --no-cache --virtual build_pack \
    bash \
    git \
    glide \
    protobuf \
    && go get github.com/2tvenom/go-test-teamcity \
    && go get -u github.com/golang/protobuf/protoc-gen-go

COPY entrypoint.sh $SRC/entrypoint.sh

VOLUME $CODE

WORKDIR $SRC

ENTRYPOINT ./entrypoint.sh
