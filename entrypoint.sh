#!/usr/bin/env bash
#colors
color_success="\\033[1;32m"
color_failure="\\033[1;31m"
color_normal="\\033[0;39m"

cp -rf ${CODE} ./${NAME}
cd ${NAME}

echo -e "$color_success""Install dependencies""$color_normal"
glide up
if [[ "${PROTODIR}" != "" ]];then
    echo -e "$color_success""Generate proto files""$color_normal"
    protoc -I ./ ./proto/${PROTODIR}/*.proto --go_out=plugins=grpc:./
fi
echo -e "$color_success""Start tests""$color_normal"
go test -v $(go list ./... | grep -v /vendor/) | go-test-teamcity